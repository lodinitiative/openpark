# -*- coding: utf-8 -*-
Leaflet.tile_layer = "http://cyberjapandata.gsi.go.jp/xyz/std/{z}/{x}/{y}.png"
#Leaflet.tile_layer = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png'
Leaflet.attribution = '<a href="http://portal.cyberjapan.jp/help/termsofuse.html" target="_blank">国土地理院</a>'
Leaflet.max_zoom = 18
